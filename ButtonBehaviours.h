#ifndef BUTTON_BEHAVIOUR_h
#define BUTTON_BEHAVIOUR_h
#include "Arduino.h"
#include "Behaviours.h"
#include <VizBlocks.h>

class ButtonPressed : public Behaviour {
  /*
   * Class that defines a behaviour that publishes a
   * ButtonPressed message to the input topic of the
   * MQQT broker
   */
   VizBlocks* _node;

public:
  ButtonPressed(VizBlocks* node, String name = "ButtonPressed") :
    Behaviour(name), _node(node){ }

  char* args() {return "<String buttonId>"; };

  String start(String args) {
    //This is where you do your stuff for a simple behaviour
    String str = "{\"id\":\"" + String(_node->getId()) + "\",\"Input\":{\"type\":\"" + String(name()) + "\",\"button\":\"" + args + "\"}}";
    _node->announce(str);
    return "ButtonPressed behaviour " + _name + " with (" + args + ")";
  }
};

class ButtonReleased : public Behaviour {
  /*
   * Class that defines a behaviour that publishes a
   * ButtonReleased message to the input topic of the
   * MQQT broker
   */
   VizBlocks* _node;

public:
  ButtonReleased(VizBlocks* node, String name = "ButtonReleased") :
    Behaviour(name), _node(node){ }

  char* args() {return "<String buttonId>"; };

  String start(String args) {
    //This is where you do your stuff for a simple behaviour
    String str = "{\"id\":\"" + String(_node->getId()) + "\",\"Input\":{\"type\":\"" + String(name()) + "\",\"button\":\"" + args + "\"}}";
    _node->announce(str);
    return "ButtonReleased behaviour " + _name + " with (" + args + ")";
  }
};

class ButtonClicked : public Behaviour {
  /*
   * Class that defines a behaviour that publishes a
   * ButtonClicked message to the input topic of the
   * MQQT broker
   */
   VizBlocks* _node;

public:
  ButtonClicked(VizBlocks* node, String name = "ButtonClicked") :
    Behaviour(name), _node(node){ }

  char* args() {return "<String buttonId>"; };

  String start(String args) {
    //This is where you do your stuff for a simple behaviour
    String str = "{\"id\":\"" + String(_node->getId()) + "\",\"Input\":{\"type\":\"" + String(name()) + "\",\"button\":\"" + args + "\"}}";
    _node->announce(str);
    return "ButtonClicked behaviour " + _name + " with (" + args + ")";
  }
};

class ButtonHeld : public Behaviour {
  /*
   * Class that defines a behaviour that publishes a
   * ButtonLongPressed message to the input topic of the
   * MQQT broker
   */
   VizBlocks* _node;

public:
  ButtonHeld(VizBlocks* node, String name = "ButtonHeld") :
    Behaviour(name), _node(node){ }

  char* args() {return "<String buttonId>"; };

  String start(String args) {
    //This is where you do your stuff for a simple behaviour
    String str = "{\"id\":\"" + String(_node->getId()) + "\",\"Input\":{\"type\":\"" + String(name()) + "\",\"button\":\"" + args + "\"}}";
    _node->announce(str);
    return "ButtonHeld behaviour " + _name + " with (" + args + ")";
  }
};

class ButtonTick : public Behaviour {
  /*
   * Class that defines a behaviour that publishes a
   * ButtonRepeatPressed message to the input topic of the
   * MQQT broker
   */
   VizBlocks* _node;

public:
  ButtonTick(VizBlocks* node, String name = "ButtonTick") :
    Behaviour(name), _node(node){ }

  char* args() {return "<String buttonId>"; };

  String start(String args) {
    //This is where you do your stuff for a simple behaviour
    String str = "{\"id\":\"" + String(_node->getId()) + "\",\"Input\":{\"type\":\"" + String(name()) + "\",\"button\":\"" + args + "\"}}";
    _node->announce(str);
    return "ButtonTick behaviour " + _name + " with (" + args + ")";
  }
};

#endif
