#ifndef LED_BEHAVIOUR_h
#define LED_BEHAVIOUR_h
#include "Arduino.h"
#include "Behaviours.h"

#include <Adafruit_NeoPixel.h>

class NumLEDs : public Behaviour {
  Adafruit_NeoPixel* _strip;
  uint32_t _color;

public:
  NumLEDs(Adafruit_NeoPixel* strip, String name = "NumLEDs", uint32_t color=0xFFFFFFFF) :
      Behaviour(name), _strip(strip), _color(color){ }
  char* args() {return "<int num_leds>"; };

  String start(String args) {
    int val = args.toInt();
    //Always clear the strip first
    _strip->clear();
    if( val > 0 ) {
      _strip->fill(_color, 0, val);
    }
    _strip->show();
    return "";
  }

};

class BrightnessLEDs : public Behaviour {
  Adafruit_NeoPixel* _strip;
  uint32_t _hue;
  uint32_t _sat;

public:
  BrightnessLEDs(Adafruit_NeoPixel* strip, String name = "BrightnessLEDs", uint32_t hue=0, uint32_t sat=0) :
      Behaviour(name), _strip(strip), _hue(hue), _sat(sat){ }
  char* args() {return "<int brightness>"; };

  String start(String args) {
    int val = args.toInt();
    _strip->clear();
    _strip->fill(_strip->ColorHSV(_hue,_sat,val));
    _strip->show();
    return "";
  }

};

class BreathingLEDs : public Behaviour {
  Adafruit_NeoPixel* _strip;
  uint _hue;
  uint _sat;
  int32_t _current = 0;
  //Allows us to have slightly slower behaviours on the go...
  int _factor = 4;
  int _rate = 0;
  int _direction = 1;

public:
  BreathingLEDs(Adafruit_NeoPixel* strip, String name = "BreathingLEDs", uint32_t hue=0, uint32_t sat=0) :
      Behaviour(name), _strip(strip), _hue(hue * 255), _sat(sat) { }
  char* args() {return "<int rate (1-255ish)>"; };

  String start(String args) {
    _current = 0;
    _direction = 1;
    _running = true;
    int val = args.toInt();
    _rate = val;
    return "";
  }

  void update() {
    if( _rate <= 0 ) {
      _strip->fill(0);
      _strip->show();
      return;
    }
    _current = _current + (_rate * _direction);
    if( _current < 0 ) {
      _current = 0;
      _direction = 1;
    }
    if( _current > 255 * _factor ) {
      _current = 255 * _factor;
      _direction = -1;
    }
    _strip->fill(_strip->ColorHSV(_hue,_sat,_current / _factor));
    _strip->show();

  }

};


#endif
