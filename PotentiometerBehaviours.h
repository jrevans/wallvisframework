#ifndef POTENTIOMETER_BEHAVIOUR_h
#define POTENTIOMETER_BEHAVIOUR_h
#include "Arduino.h"
#include "Behaviours.h"
#include <VizBlocks.h>

class PotentiometerUpdated : public Behaviour {
  /*
   * Class that defines a behaviour that publishes a
   * ButtonPressed message to the input topic of the
   * MQQT broker
   */
   VizBlocks* _node;

public:
  PotentiometerUpdated(VizBlocks* node, String name = "PotentiometerUpdated") :
    Behaviour(name), _node(node){ }

  char* args() {return "<String potentiometerId> <int value>"; };

  String start(String args) {
    //This is where you do your stuff for a simple behaviour
    int index = args.indexOf(" ");
    String pot = "";
    String value = "";

    if ( index > 0 ) {
      pot = args.substring(0, index);
      value = args.substring(index+1);
    } else {
      return "PotentiometerUpdated behaviour args error!";
    }
    String str = "{\"id\":\"" + String(_node->getId()) + "\",\"Input\":{\"type\":\"" + String(name()) + "\",\"pot\":\"" + pot + "\",\"value\":\"" + value + "\"}}";
    _node->announce(str);
    return "PotentiometerUpdated behaviour " + _name + " with (" + args + ")";
  }
};

#endif
