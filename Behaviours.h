#ifndef BEHAVIOUR_h
#define BEHAVIOUR_h

class Behaviour {
protected:
  boolean _interruptable = true;
  boolean _temp = false;
  boolean _priority = false;
  boolean _running = false;
  boolean _background = false;
  String _name = "name";

public:
  Behaviour(String name) : _name(name) {};
  ~Behaviour() {};

  //Can this behaviour be interruped
  virtual boolean is_interruptable() { return _interruptable; };
  //Can this behaviour be run quickly without stopping what's going on (e.g. comms, debug)
  virtual boolean is_temp() { return _temp; };
  //Should this behaviour override others
  virtual boolean is_priority() { return _priority; };
  //Is the behaviour running
  virtual boolean is_running() { return _running; };
  //What's the name of this behaviour
  virtual boolean is_background() { return _background; };
  //What's the name of this behaviour
  virtual String name() { return _name; };
  //What arguments does the behaviour take? Override this to document your behaviour
  virtual char* args() { return "null"; };
  //Start the behaviour, with arguments (don't know why this can't be virtual?)
  virtual String start(String args) { Serial.println("Base start called <"+args+">"); };
  //Update the behaviour periodically
  virtual void update() { };
  //Start the behaviour, with arguments (don't know why this can't be virtual?)
  virtual void stop() { _running = false; };
};

/*
 * Example way to make a simple behaviour
 */
class TestBehaviour : public Behaviour {
public:
  TestBehaviour(String n) : Behaviour(n)  {}
  String start(String args) {
    return "Test behaviour " + _name + " with (" + args + ")";
  }
};


class BehaviourTable {
  Behaviour* behaviours[40];

public:
  int num = 0;

  BehaviourTable() {}

  void add(Behaviour *b) {
    behaviours[num] = b;
    num++;
  }

  Behaviour* get(String n) {
    for( int i = 0; i < num; i++ ) {
      if( behaviours[i]->name() == n) { return behaviours[i]; }
    }
    return nullptr;
  }

  Behaviour* get_by_num(int n) {
    return behaviours[n];
  }

  int get_num_behaviours() {
    return num;
  }
};

#endif
