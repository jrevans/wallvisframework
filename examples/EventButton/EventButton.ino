#include "Button.h"

const int pin = 2;
const int ID = 0;

Button b(pin, ID);

void ICACHE_RAM_ATTR ISR() {
  b.tick();
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  b.initInterrupts(ISR);
  b.setEventHandler(cb);
  delay(500);
  
  Serial.println("\n\nESP8266 Button Test:");
}

void loop() {
  // put your main code here, to run repeatedly:
  b.check();
}

void cb(Button* button, uint8_t eventType, bool state) {
  /*
   * Rotary Encoder event handler that triggers VizBlocks behaviours
   */
   
  String idString = String(button->getId());

  Serial.println("Button ID: " + idString + " Event Type: " + String(eventType) + " State: " + String(state));

  switch(eventType) {
    case Button::kEventPressed:
    //Do something
    break;
    
    case Button::kEventReleased:
    //Do something else
    break;

    case Button::kEventClicked:
    //Do something else
    break;

    case Button::kEventHeld:
    //Do something else
    break;

    case Button::kEventTick:
    //Do something else
    break;
  }
}
