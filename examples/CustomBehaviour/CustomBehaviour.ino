#include <VizBlocks.h>
#include <Behaviours.h>

VizBlocks node(
  "new001",     // Our ID
  "VizBlocksNet", //Wifi Access Point
  "VizBlocksAP",  //WiFi Password
  "192.168.4.1",//IP address of Node RED server
  1883          //Port for Node RED server
  );

/*
 * Class that defines a custom behaviour
 */
class CustomBehaviour : public Behaviour {
public:
  CustomBehaviour(String name) : Behaviour(name)  {}

  /*
   * This is the bit that actually does something!
   * args has all of the information that you are given in it, i.e. if
   * your command is called 'custom', and the command was "custom 50",
   * args would be "50"
   */
  String start(String args) {
    //This is where you do your stuff for a simple behaviour
    return "Test behaviour " + _name + " with (" + args + ")";
  }
};

void setup()
{
  //Get the serial port ready
  Serial.begin(115200);

  //Add in three different versions of the servo wiggling, with different speed parameters
  node.add(new CustomBehaviour("custom") );

  //Initialise the whole infrastructure
  node.init();
}


void loop()
{
  //Should be all we have to do
  node.vis_loop();
}
