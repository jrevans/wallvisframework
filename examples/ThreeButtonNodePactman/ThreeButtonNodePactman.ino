#include "NameDictionary.h"
#include "Button.h"
#include <VizBlocks.h>
#include <Behaviours.h>

//Get device name
const String device_id = String(ESP.getChipId(), HEX);
NameDictionary d;
const String name = d.get(device_id);
char _name[10];

//Init as a VizBlock node with "null" ID.
VizBlocks node(
  "null",     // Our ID
  "NodeRedServer", //Wifi Access Point
  "NodeR3dAP",  //WiFi Password
  "192.168.4.1",//IP address of Node RED server
  1883          //Port for Node RED server
  );

//Init buttons on pins D2, D3, and D4
Button b0(D2, 0);
Button b1(D3, 1);
Button b2(D4, 2);

//Button Interrupt Service Routines
void ICACHE_RAM_ATTR D2_interrupt() { b0.tick(); }
void ICACHE_RAM_ATTR D3_interrupt() { b1.tick(); }
void ICACHE_RAM_ATTR D4_interrupt() { b2.tick(); }
// Pin interrupts can only use global or static functions as callbacks,
// so we have to create these wrappers, even though they are ugly :(

unsigned long t = 0;
const int pingInterval = 4000;

void setup()
{
  //Get the serial port ready
  Serial.begin(115200);
  Serial.println("Serial started!");
  delay(500);

  //Pass event handlers into buttons
  b0.initInterrupts(D2_interrupt);
  b0.setEventHandler(bCB);
  b1.initInterrupts(D3_interrupt);
  b1.setEventHandler(bCB);
  b2.initInterrupts(D4_interrupt);
  b2.setEventHandler(bCB);

  //Print device name
  if (name == device_id) {
    Serial.println("!!! This device doesn't have a name yet. Let's call it: " + name);
  } else {
    Serial.println("Device name: " + name);
  }
  name.toCharArray(_name, 10);
  node.setID(_name);

  //Add in the custom behaviour we defined earlier.
  node.add(new SendCapabilities(&node) );
  node.add(new PingServer(&node) );
  node.add(new Link(&node) );

  node.add(new ButtonPressed(&node) );
  node.add(new ButtonReleased(&node) );
  node.add(new ButtonClicked(&node) );
  node.add(new ButtonHeld(&node) );
  node.add(new ButtonTick(&node) );

  //Initialise the whole infrastructure
  node.set_wifi(true);
  node.init();
  delay(500);

  node.process("PingServer");
}

void loop()
{
  node.run();
  b0.check();
  b1.check();
  b2.check();
  pingComms(pingInterval);
}

void pingComms(int interval) {
  unsigned long timeNow = millis();
  if (timeNow > t + interval) {
    t = timeNow;
    Serial.println("Link " + name);
  }
}

void bCB(Button* button, uint8_t eventType, bool state) {
  /*
   * Button event handler that triggers VizBlocks behaviours
   */

  String idString = String(button->getId());

  Serial.println("Button ID: " + idString + " Event Type: " + String(eventType) + " State: " + String(state));

  switch(eventType) {
    case Button::kEventPressed:
    //Do something
    break;

    case Button::kEventReleased:
    //Do something else
    node.input_event("ButtonReleased " + idString);
    break;

    case Button::kEventClicked:
    //Do something else
    node.input_event("ButtonClicked " + idString);
    break;

    case Button::kEventHeld:
    //Do something else
    node.input_event("ButtonHeld " + idString);
    break;

    case Button::kEventTick:
    //Do something else
    node.input_event("ButtonTick " + idString);
    break;
  }
}
