#include <AceButton.h>
#include <AdjustableButtonConfig.h>
#include <ButtonConfig.h>

#include <VizBlocks.h>

VizBlocks node(
  "new001",     // Our ID
  "JR", //Wifi Access Point
  "hotsp0t2",  //WiFi Password
  "172.20.10.8",//IP address of Node RED server
  1883          //Port for Node RED server
  );

int counter = 0;

/*
 * Class that defines a custom behaviour
 */
class Input : public Behaviour {
public:
  Input(String name) : Behaviour(name)  {}

  /*
   * This is the bit that actually does something!
   * args has all of the information that you are given in it, i.e. if
   * your command is called 'custom', and the command was "custom 50",
   * args would be "50"
   */
  String start(String args) {
    //This is where you do your stuff for a simple behaviour
    node.input_publish("input " + args);
    return "Input behaviour " + _name + " with (" + args + ")";
  }
};

void setup()
{

  //Get the serial port ready
  Serial.begin(115200);

  //Add in a test behaviour that responds to the command 'hello'
  node.add(new TestBehaviour("hello") );
  node.add(new Input("input") );
  
 
  //Initialise the whole infrastructure
  node.set_wifi(true);
  node.init();
}


void loop()
{
  //Should be all we have to do
  node.vis_loop();

  counter++;
  if (counter >= 100) {
    node.input_event("input buttonpress");
    counter = 0;
  }
}
