#include "NameDictionary.h"

const String device_id = String(ESP.getChipId(), HEX);
String name;

NameDictionary d;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  Serial.println("\n\nESP8266 ID Look Up Test");
  Serial.println("Device ID: " + device_id);

  String name = d.get(device_id);

  if (name == "null") {
    name = device_id;
    Serial.print("!!! This device doesn't have a name yet. Let's call it: " + name);
  } else {
    Serial.print("Device name: " + name);
  }
  
}

void loop() {
  // put your main code here, to run repeatedly:

}
