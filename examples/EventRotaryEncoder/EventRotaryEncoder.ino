#include "RotaryEncoder.h"

const int pinA = 4;
const int pinB = 5;
const int ID = 0;

RotaryEncoder re(pinA, pinB, ID);

void ICACHE_RAM_ATTR ISR() {
  re.tick();
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  re.initInterrupts(ISR);
  re.setEventHandler(cb);
  delay(500);
  
  Serial.println("\n\nESP8266 Rotary Encoder Test:");
}

void loop() {
  re.check();
}

void cb(RotaryEncoder* rotaryEncoder, uint8_t eventType, int position) {
  /*
   * Rotary Encoder event handler that triggers VizBlocks behaviours
   */
   
  String idString = String(rotaryEncoder->getId());

  Serial.println("Encoder ID: " + idString + " Event Type: " + String(eventType) + " Position: " + String(position));

  switch(eventType) {
    case RotaryEncoder::kEventStableUpdate:
    //Do something
    break;
    
    case RotaryEncoder::kEventUnstableUpdate:
    //Do something else
    break;
  }
}
