#include "Potentiometer.h"

const String device_id = String(ESP.getChipId(), HEX);

const int sensorPin = A0;

Potentiometer pot(sensorPin, 0);

void setup() {
  Serial.begin(115200);
  pot.setEventHandler(cb);
  delay(500);

  Serial.println("\n\nESP8266 Potentiometer Test");
  Serial.println("\nThis is board: " + device_id + "\n");
}

void loop() {
  pot.check();
}

void cb(Potentiometer* potentiometer, uint8_t eventType, uint8_t sensorValue) {
  /*
   * Potentiometer Event Handler that triggers VizBlocks behaviours
   */
  
  String idString = String(potentiometer->getId());

  Serial.println("Slider ID: " + idString + " Event Type: " + String(eventType) + " Sensor Value: " + String(sensorValue));

  switch(eventType) {
    case Potentiometer::kEventStableUpdate:
    //Do something
    break;
    
    case Potentiometer::kEventUnstableUpdate:
    //Do something else
    break;
  }
}
