#include "NameDictionary.h"
#include <Servo.h>
#include <VizBlocks.h>

//Get device name
const String device_id = String(ESP.getChipId(), HEX);
NameDictionary d;
const String name = d.get(device_id);
char _name[10];

// Set up the servo we will use
Servo s1 = Servo();

// Set up LEDs as well
#define LED_COUNT 12
#define LED_PIN    D2
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

VizBlocks node(
  "null",     // Our ID
  "NodeRedServer", //Wifi Access Point
  "NodeR3dAP",  //WiFi Password
  "192.168.4.1",//IP address of Node RED server
  1883          //Port for Node RED server
  );

void setup()
{
  // Attach the servo to the right pin on the board
  s1.attach(D3);

  // Start the LED library
  strip.begin();
  strip.fill(strip.Color(30,255,80));
  strip.show();

  delay(100);
  strip.fill(0);
  strip.show();

  //Get the serial port ready
  Serial.begin(115200);
  Serial.println("Serial started!");

  delay(500);

  //Print device name
  if (name == device_id) {
    Serial.println("!!! This device doesn't have a name yet. Let's call it: " + name);
  } else {
    Serial.println("Device name: " + name);
  }

  name.toCharArray(_name, 10);
  node.setID(_name);

  //Add in a test behaviour that responds to the command 'hello'
  node.add(new TestBehaviour("hello") );

  //Add in three different versions of the servo wiggling, with different speed parameters
  node.add(new ServoWiggle(s1, "wiggle") );
  node.add(new ServoWiggle(s1, "slow_wiggle", 10) );
  node.add(new ServoWiggle(s1, "fast_wiggle", 1) );

  //Add in a behaviour that just goes to a certain angle, with the default name 'goto'
  node.add(new ServoGoto(s1) );

  //Add in a behaviour that rotates from 0 to 180 and back again (e.g. dropping a ball trustball style)
  node.add(new RotateReturn(s1,"drop",1,500, 3, 175) );

  //A few useful LED behaviours
  node.add(new NumLEDs(&strip, "green_leds", strip.Color(10, 255, 15) ) );
  node.add(new NumLEDs(&strip, "red_leds", strip.Color(255, 30, 40) ));
  node.add(new NumLEDs(&strip, "leds"));
  node.add(new BrightnessLEDs(&strip, "red", 0, 255 ));
  node.add(new BrightnessLEDs(&strip, "green", 120, 255 ));
  node.add(new BreathingLEDs(&strip, "breathe", 120, 0 ));
  node.add(new BreathingLEDs(&strip, "breathe_blue", 170, 255 ));

  //Initialise the whole infrastructure
  node.set_wifi(true);
  node.init();

  delay(500);

  Serial.println("PactVis Outputs Test:");
}


void loop()
{
  //Should be all we have to do
  node.vis_loop();
}
