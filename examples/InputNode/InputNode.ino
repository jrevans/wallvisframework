#include "NameDictionary.h"
#include "Button.h"
#include "Potentiometer.h"
#include "RotaryEncoder.h"
#include "VizBlocks.h"
#include "Behaviours.h"

//Get device name
const String device_id = String(ESP.getChipId(), HEX);
NameDictionary d;
const String name = d.get(device_id);
char _name[10];

//Init button
const int bPin = 2;
const int bID = 0;
Button b(bPin, bID);

//Init potientiometer
const int pPin = A0;
const int pID = 1;
Potentiometer p(pPin, pID);

//Init rotary encoder
const int rePinA = 4;
const int rePinB = 5;
const int reID = 2;
RotaryEncoder re(rePinA, rePinB, reID);

//Construct VizBlocks
VizBlocks node(
  "null",     // Our ID
  "NodeRedServer", //Wifi Access Point
  "NodeR3dAP",  //WiFi Password
  "192.168.4.1",//IP address of Node RED server
  1883          //Port for Node RED server
  );

//Interrupt Serivce Routine
void ICACHE_RAM_ATTR bISR() {
  b.tick();
}

void ICACHE_RAM_ATTR reISR() {
  re.tick();
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("Serial started!");

  //Pass event handlers into button
  b.initInterrupts(bISR);
  b.setEventHandler(bCB);

  //Pass event handlers into potentiometer
  p.setEventHandler(pCB);

  //Pass event handlers into rotary encoder
  re.initInterrupts(reISR);
  re.setEventHandler(reCB);

  delay(500);

  //Print device name
  if (name == device_id) {
    Serial.println("!!! This device doesn't have a name yet. Let's call it: " + name);
  } else {
    Serial.println("Device name: " + name);
  }

  name.toCharArray(_name, 10);
  node.setID(_name);

  //Node behaviours:
  node.add(new SendCapabilities(&node) );
  node.add(new ButtonClicked(&node, "ButtonClicked") );
  node.add(new ButtonHeld(&node, "ButtonHeld") );
  node.add(new ButtonTick(&node, "ButtonTick") );
  node.add(new ButtonReleased(&node, "ButtonReleased") );
  node.add(new PotentiometerUpdated(&node, "PotentiometerUpdated") );
  node.add(new RotaryEncoderUpdated(&node, "RotaryEncoderUpdated") );


  //Initialise the whole infrastructure
  node.set_wifi(true);
  node.init();

  delay(500);

  Serial.println("PactVis Inputs Test:");
}

void loop() {
  // put your main code here, to run repeatedly:
  node.vis_loop();
  b.check();
  p.check();
  re.check();
}

void bCB(Button* button, uint8_t eventType, bool state) {
  /*
   * Button event handler that triggers VizBlocks behaviours
   */

  String idString = String(button->getId());

  Serial.println("Button ID: " + idString + " Event Type: " + String(eventType) + " State: " + String(state));

  switch(eventType) {
    case Button::kEventPressed:
    //Do something
    break;

    case Button::kEventReleased:
    //Do something else
    node.input_event("ButtonReleased " + idString);
    break;

    case Button::kEventClicked:
    //Do something else
    node.input_event("ButtonClicked " + idString);
    break;

    case Button::kEventHeld:
    //Do something else
    node.input_event("ButtonHeld " + idString);
    break;

    case Button::kEventTick:
    //Do something else
    node.input_event("ButtonTick " + idString);
    break;
  }
}

void pCB(Potentiometer* potentiometer, uint8_t eventType, uint8_t sensorValue) {
  /*
   * Potentiometer Event Handler that triggers VizBlocks behaviours
   */

  String idString = String(potentiometer->getId());

  Serial.println("Slider ID: " + idString + " Event Type: " + String(eventType) + " Sensor Value: " + String(sensorValue));

  switch(eventType) {
    case Potentiometer::kEventStableUpdate:
    //Do something
    node.input_event("PotentiometerUpdated " + idString + " " + String(sensorValue));
    break;

    case Potentiometer::kEventUnstableUpdate:
    //Do something else
    break;
  }
}

void reCB(RotaryEncoder* rotaryEncoder, uint8_t eventType, int position) {
  /*
   * Rotary Encoder event handler that triggers VizBlocks behaviours
   */

  String idString = String(rotaryEncoder->getId());

  Serial.println("Encoder ID: " + idString + " Event Type: " + String(eventType) + " Position: " + String(position));

  switch(eventType) {
    case RotaryEncoder::kEventStableUpdate:
    //Do something
    node.input_event("RotaryEncoderUpdated " + idString + " " + String(position));
    break;

    case RotaryEncoder::kEventUnstableUpdate:
    //Do something else
    break;
  }
}
