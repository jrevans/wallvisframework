#include <VizBlocks.h>
#include <Behaviours.h>

VizBlocks node(
  "new001",     // Our ID
  "VizBlocksNet", //Wifi Access Point
  "VizBlocksAP",  //WiFi Password
  "192.168.4.1",//IP address of Node RED server
  1883          //Port for Node RED server
  );

/*
 * Class that defines a custom behaviour wiht a long loop that
 * can be interrupted
 */
class LoopingBehaviour : public Behaviour {
  int _num_loops = 5;
  int _loop_counter = 0;
  int _loop_length = 100;
public:
  LoopingBehaviour(String name, int num_loops=100) : Behaviour(name), _num_loops(num_loops)  {}
  String start(String args) {
    _num_loops = args.toInt();
    _loop_counter = 0;
    _running = true;
    return "Test behaviour " + _name + " with (" + args + ")";
  }

  //This is where the looping stuff actually happens
  void update() {
    Serial.print("Loop: " );
    Serial.print(_loop_counter);
    for( int i = 0; i < _loop_length; i++ ) {
      Serial.print(".");
      //do something
      delay(200);
    }
    Serial.println();
    _loop_counter++;
    if( _loop_counter > _num_loops) {
      _running = false; // This says the behaviour is finished, and it will stop
    }
  }
};

void setup()
{
  //Get the serial port ready
  Serial.begin(115200);

  //Add in three different versions of the servo wiggling, with different speed parameters
  node.add(new LoopingBehaviour("loop", 50) );

  //Initialise the whole infrastructure
  node.init();
}


void loop()
{
  //Should be all we have to do
  node.vis_loop();
}
