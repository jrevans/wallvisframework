#include "NameDictionary.h"
#include "Button.h"
#include "Potentiometer.h"
#include "RotaryEncoder.h"
#include <VizBlocks.h>
#include <Behaviours.h>

//Get device name
const String device_id = String(ESP.getChipId(), HEX);
NameDictionary d;
const String name = d.get(device_id);
char _name[10];

//Init as a VizBlock node with "null" ID.
VizBlocks node(
  "null",     // Our ID
  "NodeRedServer", //Wifi Access Point
  "NodeR3dAP",  //WiFi Password
  "192.168.4.1",//IP address of Node RED server
  1883          //Port for Node RED server
  );

//Init button
const int bPin = D2;
const int bID = 0;
Button b(bPin, bID);

//Button Interrupt Service Routine
void ICACHE_RAM_ATTR bISR() {
  b.tick();
}

//Init potientiometer
const int pPin = A0;
const int pID = 0;
Potentiometer p(pPin, pID);

//Init rotary encoder
const int rePinA = D4;
const int rePinB = D5;
const int reID = 0;
RotaryEncoder re(rePinA, rePinB, reID);

//Rotary Encoder Interrupt Serivce Routine
void ICACHE_RAM_ATTR reISR() {
  re.tick();
}

unsigned long t = 0;
const int pingInterval = 4000;

void setup()
{
  //Get the serial port ready
  Serial.begin(115200);
  Serial.println("Serial started!");
  delay(500);

  //Pass event handlers into button
  b.initInterrupts(bISR);
  b.setEventHandler(bCB);
  //Pass event handlers into potentiometer
  p.setEventHandler(pCB);
  //Pass event handlers into rotary encoder
  re.initInterrupts(reISR);
  re.setEventHandler(reCB);

  //Print device name
  if (name == device_id) {
    Serial.println("!!! This device doesn't have a name yet. Let's call it: " + name);
  } else {
    Serial.println("Device name: " + name);
  }
  name.toCharArray(_name, 10);
  node.setID(_name);

  //Add in the custom behaviour we defined earlier.
  node.add(new SendCapabilities(&node) );
  node.add(new PingServer(&node) );
  node.add(new Link(&node) );

  node.add(new ButtonPressed(&node) );
  node.add(new ButtonReleased(&node) );
  node.add(new ButtonClicked(&node) );
  node.add(new ButtonHeld(&node) );
  node.add(new ButtonTick(&node) );
  node.add(new PotentiometerUpdated(&node) );
  node.add(new RotaryEncoderUpdated(&node) );

  //Initialise the whole infrastructure
  node.set_wifi(true);
  node.init();
  delay(500);

  node.process("PingServer");
}

void loop()
{
  node.run();
  b.check();
  p.check();
  re.check();
  pingComms(pingInterval);
}

void pingComms(int interval) {
  unsigned long timeNow = millis();
  if (timeNow > t + interval) {
    t = timeNow;
    Serial.println("Link " + name);
  }
}

void bCB(Button* button, uint8_t eventType, bool state) {
  /*
   * Button event handler that triggers VizBlocks behaviours
   */

  String idString = String(button->getId());

  Serial.println("Button ID: " + idString + " Event Type: " + String(eventType) + " State: " + String(state));

  switch(eventType) {
    case Button::kEventPressed:
    //Do something
    break;

    case Button::kEventReleased:
    //Do something else
    node.input_event("ButtonReleased " + idString);
    break;

    case Button::kEventClicked:
    //Do something else
    node.input_event("ButtonClicked " + idString);
    break;

    case Button::kEventHeld:
    //Do something else
    node.input_event("ButtonHeld " + idString);
    break;

    case Button::kEventTick:
    //Do something else
    node.input_event("ButtonTick " + idString);
    break;
  }
}

void pCB(Potentiometer* potentiometer, uint8_t eventType, uint8_t sensorValue) {
  /*
   * Potentiometer Event Handler that triggers VizBlocks behaviours
   */

  String idString = String(potentiometer->getId());

  Serial.println("Slider ID: " + idString + " Event Type: " + String(eventType) + " Sensor Value: " + String(sensorValue));

  switch(eventType) {
    case Potentiometer::kEventStableUpdate:
    //Do something
    node.input_event("PotentiometerUpdated " + idString + " " + String(sensorValue));
    break;

    case Potentiometer::kEventUnstableUpdate:
    //Do something else
    break;
  }
}

void reCB(RotaryEncoder* rotaryEncoder, uint8_t eventType, int position) {
  /*
   * Rotary Encoder event handler that triggers VizBlocks behaviours
   */

  String idString = String(rotaryEncoder->getId());

  Serial.println("Encoder ID: " + idString + " Event Type: " + String(eventType) + " Position: " + String(position));

  switch(eventType) {
    case RotaryEncoder::kEventStableUpdate:
    //Do something
    node.input_event("RotaryEncoderUpdated " + idString + " " + String(position));
    break;

    case RotaryEncoder::kEventUnstableUpdate:
    //Do something else
    break;
  }
}
