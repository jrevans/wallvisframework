#ifndef NAMEDICTIONARY_h
#define NAMEDICTIONARY_h

typedef struct {
  String key;
  String value;
} keyValuePair;

class NameDictionary {

  keyValuePair data[50] = {
    {"c2c373", "abe"},
    {"c31d9d", "aja"},
    {"c2b603", "ace"},
    {"da58f5", "ali"},
    {"da6195", "alf"},
    {"da50d8", "amy"},
    {"da5649", "ann"},
    {"c2b2d6", "art"},
    {"da516d", "aya"},
    {"c2a597", "bea"},
    {"da5331", "bev"},
    {"8e07b",  "bob"},
    {"c318a1", "bud"},
    {"da5a56", "deb"},
    {"da4a70", "cal"},
    {"c2c5c2", "cam"},
    {"c2a5e6", "che"},
    {"c2a23f", "dot"},
    {"c2c415", "dan"},
    {"c2bf2a", "dax"},
    {"30d6ed", "fin"},
    {"30d00b", "jim"},
    {"1a6b16", "joe"},
    {"8810e9", "kat"},
    {"31054b", "kev"},
    {"30ce48", "sam"},
    {"30db8b", "tim"},
  };

  int _arraySize;

  public:
    NameDictionary() {
      _arraySize = sizeof(data) / sizeof(keyValuePair);
    };

    String get(String key) {
      for (int i = 0; i < _arraySize; i++) {
        if (key == data[i].key) {
          return data[i].value;
        }
      }
      return key;
    };

    void list() {
      for (int i = 0; i < _arraySize; i++) {
        Serial.println(data[i].key + " : " + data[i].value);
      }
    };

    void values() {
      for (int i = 0; i < _arraySize; i++) {
        Serial.println(data[i].value);
      }
    };

    void keys() {
      for (int i = 0; i < _arraySize; i++) {
        Serial.println(data[i].key);
      }
    };

    int length() {
      return _arraySize;
    }
};

#endif
