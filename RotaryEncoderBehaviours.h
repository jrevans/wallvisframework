#ifndef ROTARYENCODER_BEHAVIOUR_h
#define ROTARYENCODER_BEHAVIOUR_h
#include "Arduino.h"
#include "Behaviours.h"
#include <VizBlocks.h>

class RotaryEncoderUpdated : public Behaviour {
  /*
   * Class that defines a behaviour that publishes a
   * ButtonPressed message to the input topic of the
   * MQQT broker
   */
   VizBlocks* _node;

public:
  RotaryEncoderUpdated(VizBlocks* node, String name = "RotaryEncoderUpdated") :
    Behaviour(name), _node(node){ }

  char* args() {return "<String rotaryEncoderId> <int position>"; };

  String start(String args) {
    //This is where you do your stuff for a simple behaviour
    int index = args.indexOf(" ");
    String encoder = "";
    String position = "";

    if ( index > 0 ) {
      encoder = args.substring(0, index);
      position = args.substring(index+1);
    } else {
      return "RotaryEncoderUpdated behaviour args error!";
    }

    String str = "{\"id\":\"" + String(_node->getId()) + "\",\"Input\":{\"type\":\"" + String(name()) + "\",\"encoder\":\"" + encoder + "\",\"position\":\"" + position + "\"}}";
    _node->announce(str);
    return "RotaryEncoderUpdated behaviour " + _name + " with (" + args + ")";
  }
};

#endif
