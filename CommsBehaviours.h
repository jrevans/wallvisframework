#ifndef COMMS_BEHAVIOUR_h
#define COMMS_BEHAVIOUR_h
#include "Arduino.h"
#include "Behaviours.h"
#include <VizBlocks.h>

/*
 * --------------------------------------------------
 * ---------------- SendCapabilities ----------------
 * --------------------------------------------------
 */

class SendCapabilities : public Behaviour {
   VizBlocks* _node;

public:
  SendCapabilities(VizBlocks* node, String name = "SendCapabilities") :
    Behaviour(name), _node(node){ }

  String start(String args) {
    //This is where you do your stuff for a simple behaviour
    _node->announce_capabilities();
    return "SendCapabilities behaviour " + _name;
  }

};

/*
 * --------------------------------------------------
 * ---------------------- Link ----------------------
 * --------------------------------------------------
 */

class Link : public Behaviour {
  VizBlocks* _node;
  String _peerId;
  const int _timeoutInterval = 5000;
  unsigned long _t = 0;

public:
  Link(VizBlocks* node, String name = "Link") : Behaviour(name), _node(node)  { _background = true; }

  char* args() {return "<String peerId>"; };

  String start(String args) {
    _running = true;
    if (args == name() || args.indexOf(" ")>0) {
      return "Invalid args (" + args + ") in behaviour " + name();
    }

    _t = millis();

    if (args == _peerId) {
      return "Link ping from (" + _peerId + ")";
    }

    _peerId = args;

    String str = "{\"id\":\"" + String(_node->getId()) + "\",\"Link\":{\"peerId\":\"" + _peerId + "\"}}";
    _node->announce(str);

    return "New link with (" + _peerId + ")";
  }

  void update() {
    if (millis() > (_t+_timeoutInterval)) {
      String str = "{\"id\":\"" + String(_node->getId()) + "\",\"Unlink\":{\"peerId\":\"" + _peerId + "\"}}";
     _node->announce(str);
      _peerId = "";
      _running = false;
    }
  }

};

/*
 * --------------------------------------------------
 * ------------------- PingServer -------------------
 * --------------------------------------------------
 */

class PingServer : public Behaviour {
  VizBlocks* _node;
  String str;
  const int _interval = 4000;
  unsigned long _t = 0;

public:
  PingServer(VizBlocks* node, String name = "PingServer") : Behaviour(name), _node(node) { _background = true; }

  String start(String args) {
    _background = true;
    _running = true;
    _t = millis();
    str = "{\"id\":\"" + String(_node->getId()) + "\",\"PingServer\":{}}";
    _node->announce(str);
    return "Pinging server";
  }

  void update() {
    if (millis() > (_t+_interval)) {
      _t = millis();
     _node->announce(str);
    }
  }

};

#endif
